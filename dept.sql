CREATE VIEW dept AS
SELECT Department.DName,Dept_Locations.LocationName
FROM Department JOIN Dept_Locations
ON Department.Dnumber = Dept_Locations.Lnumber
ORDER BY Dept_Locations.LocationName;
